# nimbleTempDev

A collection of functions for modelling arthropod development rates (mean and variance) as a function of temperature.
Adapted originally from the PhD work of Soledad Castano (http://www.theses.fr/223623849),
and extended during the masters project of Walid Kandouci.


To build and install the package, do the following

* Create a directory in which to build the package, I call mine nimbleTempDev
* cd to that directory
* git clone git@forgemia.inra.fr:umr-astre/nimbletempdev.git
* R CMD build nimbleTempDev
* R CMD check nimbleTempDev_0.1.0.tar.gz (optional - for package developers only)
* R CMD INSTALL nimbleTempDev_0.1.0.tar.gz

Alternatively, if the above fails to install the vignette, you do the following from R
* setwd("nimbleTempDev")
* grep("DESCRIPTION", dir()) # This will return 1 if you are in the correct working directory
* devtools::install(build_vignettes = TRUE)


<!------------------------------------------------------------------------------------------------------------->
<!-- It may be possible to install the package, from R, as follows...										 -->
<!-- ``` r																									 -->
<!-- if (!require("remotes")) {																				 -->
<!--   install.packages("remotes")																			 -->
<!-- }																										 -->
<!-- remotes::install_gitlab("umr-astre/nimbleTempDev", subdir = "nimbleTempDev", host = "forgemia.inra.fr") -->
<!-- ```																									 -->
<!-- Alternatively, 																						 -->
<!------------------------------------------------------------------------------------------------------------->
