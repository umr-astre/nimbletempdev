---
title: "nimbleTempDev: an introduction"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{nimbleTempDev: an introduction}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

In this vignette we will concentrate on how to use the package funcitons inside `nimble` models.
The functions can also be used as stand-alone functions, and various examples are provided in the help files.

The `nimbleTempDev` package is loaded in the usual way.
```{r setup, message=FALSE}
library(nimbleTempDev)
```


# Fixed temperature for a single stage model

```{r model1}
nimbleTempDev::TW2
model1 = nimbleCode({

})
```
