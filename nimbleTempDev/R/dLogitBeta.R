##' Beta distribution transformed to logit scale.
##'
##' Improved MCMC mixing can sometimes be possible if bounded parameters are transformed and sampled on unbounded scales.
##' These distribution functions can be used within nimble models to provide this posibility for the beta distribution.
##'
##' dLogitBeta returns the density of x where y ~ Beta(a1,a2) and x = logit(y).
##'
##' rLogitBeta simulates y ~ Beta(a1,a2) and returns x = logit(y)
##'
##' @title Logit transformed beta distribution.
##' @param x Value of logit transformed beta random variable.
##' @param shape1 Often written alpha or alpha1. First shape parameter of beta distribution.
##' @param shape2 Often written beta or alpha1. Second shape parameter of beta distribution.
##' @param log Logical. If set to 1 dLogitBeta return the log likelihood of x given the parameters.
##' @param n Number of random samples. Currently nimble only accepts n=1. If higher n are required, perhaps write and compile a wrapper nimble function.
##' @return
##'
##' For dLogitBeta returns the density, or log density, of x.
##'
##' For rLogitBeta returns a random number drawn from a beta distribution and transformed to the logit scale.
##'
##' @author David Pleydell, Soledad Castano
##' @name logitBeta
##' @examples
##' N = 1000
##' a1 = 50
##' a2 = 10
##' x = replicate(N, rLogitBeta(1, a1, a2))
##' y = ilogit(x)
##' hist(y, freq=FALSE, n=35)
##' curve(dbeta(x, a1, a2), 0, 1, n=301, col="red", lwd=2, add=TRUE)

NULL

##' @rdname logitBeta
##' @export
dLogitBeta <- nimbleFunction (
    ##
    run = function(x = double(0),
                   shape1=double(0, default=1.0),
                   shape2=double(0, default=1.0),
                   log = integer(0, default=0)) {
        returnType(double(0))
        y = ilogit(x)
        logProbX = log(y) + log(1 - y) + dbeta(y, shape1=shape1, shape2=shape2, log=TRUE) ## Via change of variable
        if (log)
            return(logProbX)
        return(exp(logProbX))
    }
)

##' @rdname logitBeta
##' @export
rLogitBeta <- nimbleFunction (
    run = function(n = integer(0, default=1),
                   shape1 = double(0, default=1.0),
                   shape2 = double(0, default=1.0)) {
        returnType(double(0))
        if(n != 1)
            nimPrint("Warning: rLogitBeta only allows n = 1; Using n = 1.\n")
        y <- rbeta(1, shape1=shape1, shape2=shape2)
        x <- logit(y)
        return(x)
    }
)
