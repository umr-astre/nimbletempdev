# This file registers all distributions when the package is loaded.
.onAttach <- function(libname, pkgname) {

  packageStartupMessage("Loading nimbleTempDev. \nRegistering the following user-defined functions:\n ",
                        "dLogitBeta", ", dJSMD.\n")

  # Register the distributions explicitly for two reasons:
  # 1. Avoid message to user about automatic registrations upon first use in a nimbleModel
  # 2. Establish default len = 0 via reparameterization mechanism.
  suppressMessages({

    ## registerDistributions(list(
    ## dCJS_ss = list(
    ##   BUGSdist = "dCJS_ss(probSurvive, probCapture, len)",
    ##   Rdist = "dCJS_ss(probSurvive, probCapture, len = 0)",
    ##   discrete = TRUE,
    ##   types = c('value = double(1)', 'probSurvive = double()', 'probCapture = double()', 'len = double()'),
    ##   pqAvail = FALSE)), verbose = F
    ## )

    registerDistributions(
      list(dLogitBeta = list(
        BUGSdist = "dLogitBeta(shape1, shape2)",
        discrete = FALSE,
        types    = c("value=double(0)"), ## , "para=double(0)"
        pqAvail  = FALSE)),
      verbose = FALSE
    )

    registerDistributions(list(dLogExp = list(
      BUGSdist = "dLogExp(rate)",
      discrete = FALSE,
      types    = c("value=double(0)"),
      pqAvail  = FALSE)),
      verbose = FALSE
      )

    registerDistributions(list(dLogGamma = list(
      BUGSdist = "dLogGamma(shape, scale)",
      discrete = FALSE,
      types    = c("value=double(0)"),
      pqAvail  = FALSE)),
      verbose = FALSE
      )

    registerDistributions(
      list(dJSMD = list(
        BUGSdist = "dJSMD(paras, res, CensorTime)",
        discrete = TRUE,
        types    = c("value=double(1)", "paras=double(1)", "res=double(0)", "CensorTime=double(0)"),
        pqAvail  = FALSE)), verbose = F
    )

})}
