For a rapid check of the vignette do this
rmarkdown::render("introduction.Rmd")

To build the package
0. Verify that the directory structure is squeeky clean. Files in the wrong place prevent the vignette from being built.
1. setwd(here::here())
2. roxygen2::roxygenise()
3. R CMD build nimbleTempDev
4. R CMD INSTALL nimbleTempDev_0.1.1.tar.gz
5. Open a new R
6. vignette(package="nimbleTempDev") # Will tell you if it is there or not
7. help.start()
